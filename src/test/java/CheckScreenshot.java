import com.google.common.collect.Iterables;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.imageio.ImageIO;

public class CheckScreenshot {

    private static BufferedImage image1, image2, imageResult;
    private static boolean isIdentic;
    private static List<String> comparisonZones = new ArrayList();
    private static List<String> ignoreZones = new ArrayList();
    private static Collection<String> listIgnore = new ArrayList<String>();
    private static Collection<String> listCompare = new ArrayList<String>();

    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    static String date = dateFormat.format(new Date());

    // a number of pieces along vertical and horizontal
    private static int compareX, compareY;

    // “tolerance” comparison parameter that allows to treat similar colors as the same
    private static double sensitivity = 0.10;

    public CheckScreenshot(String file1, String file2) throws IOException {
        image1 = loadPNG(file1);
        image2 = loadPNG(file2);
    }

    // set a number of pieces along vertical and horizontal
    public void setParameters(int compareX, int compareY) {
        CheckScreenshot.compareX = compareX;
        CheckScreenshot.compareY = compareY;
    }

    // compare the two images in this object.
    public void compare3() {
        // setup change display image
        int x = 0;
        int y = 0;
        imageResult = new BufferedImage(image2.getWidth(null), image2.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = imageResult.createGraphics();
        g2.drawImage(image2, null, null);
        g2.setColor(Color.RED);
        // assign size of each section
        int blocksX = (int) (image1.getWidth() / compareX);  //!!!
        int blocksY = (int) (image1.getHeight() / compareY); //!!!
        CheckScreenshot.isIdentic = true;
        if (comparisonZones.size() > 0) {
            for (int s = 0; s < comparisonZones.size(); s++) {
                String c = Iterables.get(comparisonZones, s);
                String[] coordinats = c.split(",");

                x = Integer.parseInt(coordinats[0]);
                y = Integer.parseInt(coordinats[1]);

                int secondX = x * blocksX;
                int secondY = y * blocksY;

                //int result1[][] = convertTo2D(image1.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                //int result2[][] = convertTo2D(image2.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                int result1[][] = convertTo2D(image1.getSubimage(secondX, secondY, blocksX, blocksY));
                int result2[][] = convertTo2D(image2.getSubimage(secondX, secondY, blocksX, blocksY));
                for (int i = 0; i < result1.length; i++){
                    for (int j = 0; j < result1[0].length; j++) {
                        int diff = Math.abs(result1[i][j] - result2[i][j]);
                        if (diff / Math.abs(result1[i][j]) > sensitivity) {
                            String newZone = Integer.toString(x) + "," + Integer.toString(y);
                            if (listIgnore.size() > 0) {
                                for (int p = 0; p < listIgnore.size(); p++) {
                                    if (listIgnore.contains(newZone)) {
                                    } else {
                                        g2.drawRect(secondX, secondY, blocksX - 1, blocksY - 1);
                                        isIdentic = false;
                                    }
                                }
                            } else {
                                g2.drawRect(secondX, secondY, blocksX - 1, blocksY - 1);
                                isIdentic = false;
                            }
                        }
                    }
                }
            }
        } else {
            for (int z = 0; z < compareY; z++) {
                for (int t = 0; t < compareX; t++) {
                    int secondX = t * blocksX;
                    int secondY = z * blocksY;

                    //int result1[][] = convertTo2D(image1.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                    //int result2[][] = convertTo2D(image2.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                    int result1[][] = convertTo2D(image1.getSubimage(secondX, secondY, blocksX, blocksY));
                    int result2[][] = convertTo2D(image2.getSubimage(secondX, secondY, blocksX, blocksY));

                    for (int i = 0; i < result1.length; i++) {
                        for (int j = 0; j < result1[0].length; j++) {
                            int diff = Math.abs(result1[i][j] - result2[i][j]);
                            if (diff / Math.abs(result1[i][j]) > sensitivity) {
                                String newZone = Integer.toString(t) + "," + Integer.toString(z);
                                if (listIgnore.size() > 0) {
                                    for (int p = 0; p < listIgnore.size(); p++) {
                                        if (listIgnore.contains(newZone)) {
                                        } else {
                                            g2.drawRect(secondX, secondY, blocksX - 1, blocksY - 1);
                                            isIdentic = false;
                                        }
                                    }
                                } else {
                                    g2.drawRect(secondX, secondY, blocksX - 1, blocksY - 1);
                                    isIdentic = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // compare the two images in this object.
    public void getZone(List<String> list) {
        // setup change display image
        imageResult = new BufferedImage(image2.getWidth(null), image2.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = imageResult.createGraphics();
        g2.drawImage(image2, null, null);
        g2.setColor(Color.RED);
        // assign size of each section
        int blocksX = (int) (image1.getWidth() / compareX);
        int blocksY = (int) (image1.getHeight() / compareY);
        CheckScreenshot.isIdentic = true;
        for (int y = 0; y < compareY; y++) {
            for (int x = 0; x < compareX; x++) {
                int secondX = x * blocksX;
                int secondY = y * blocksY;
                int result1[][] = convertTo2D(image1.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                int result2[][] = convertTo2D(image2.getSubimage(secondX, secondY, blocksX - 1, blocksY - 1));
                for (int i = 0; i < result1.length; i++) {
                    for (int j = 0; j < result1[0].length; j++) {
                        int diff = Math.abs(result1[i][j] - result2[i][j]);
                        if (diff / Math.abs(result1[i][j]) > sensitivity) {
                            String newZ = Integer.toString(x) + "," + Integer.toString(y);

                            if (list.size() == 0) {
                                list.add(newZ);
                            } else {
                                for (int f = 0; f < list.size(); f++) {
                                    if (list.contains(newZ)) {
                                    }else{
                                        list.add(newZ);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public BufferedImage getImageResult() {
        return imageResult;
    }

    // representation fragment's of the picture in a two-dimensional integer array
    public int[][] convertTo2D(BufferedImage subimage) {
        int width = subimage.getWidth();
        int height = subimage.getHeight();
        int[][] result = new int[height][width];

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                result[row][col] = subimage.getRGB(col, row);
            }
        }
        return result;
    }

    // reading picture from disk
    public static BufferedImage loadPNG(String filename){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    // writing picture into disk
    public static void savePNG(BufferedImage bimg, String filename) {
        try {
            File outputfile = new File(filename);
            ImageIO.write(bimg, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveResult(BufferedImage bimg, String imageName) throws IOException {
        image1 = loadPNG("visualTests\\images\\"+ imageName + ".png");

        try {
            File outputfile = new File("visualTests\\results\\" + imageName + " " + date + ".png");

            int width = bimg.getWidth() +2;
            int height = bimg.getHeight() +50;

            BufferedImage im = new BufferedImage((2*width), height, BufferedImage.TYPE_INT_ARGB);

            Graphics2D graphics = im.createGraphics();
            graphics.setColor(new Color(0, 0, 0 ));
            graphics.fillRect(0, 0, (2*width), height);

            im.getGraphics().drawImage(image1, 0, 0, null);
            im.getGraphics().drawImage(bimg, width+2, 0, null);

            Graphics g = im.getGraphics();
            g.setFont(g.getFont().deriveFont(40f));
            g.setColor(new Color(255, 255, 255 ));
            g.drawString("Expected", (width/2)-90, height - 10);
            g.drawString("Actual", ((width/2)*3)-90, height - 10);
            g.dispose();

            ImageIO.write(im, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isIdentic() {
        return isIdentic;
    }

    //--- Main Method
    public static void checkScreenshot(String imageName, WebDriver driver) throws IOException {
        ignoreZones.clear();
        comparisonZones.clear();
        int splitX = 25;
        int splitY = 25;

        if (!Files.exists(Paths.get("visualTests\\results"))) {
            {
                new File("visualTests\\results\\").mkdirs();
            }
        }

        //---check if there is such an image.
        System.out.println("[INFO]: Take screenshot of '" + imageName + "' ");
        //---take a screenshot
        if (!Files.exists(Paths.get("visualTests\\images\\" + imageName + ".png"))) {
            System.out.println("[INFO]: New main image for '" + imageName + "' !!!");
            try {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); //((TakesScreenshot) BrowsersHelper.getChromeRemoteWebDriver()).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File("visualTests\\images\\" + imageName + ".png"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        //---take a screenshot
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("visualTests\\screenshots\\" + imageName + ".png"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        //---get ignore zones
        if (Files.exists(Paths.get("visualTests\\images\\" + imageName + "_Ignore.png"))) {
            System.out.print("[INFO]: There are Ignore zones for '" + imageName + "' - ");
            CheckScreenshot ign = new CheckScreenshot("visualTests\\images\\" + imageName + ".png",
                    "visualTests\\images\\" + imageName + "_Ignore.png");
            ign.setParameters(splitX, splitY);
            ign.getZone(ignoreZones);
            listIgnore = new ArrayList<String>(ignoreZones);
            System.out.println(listIgnore.size());
        } else {
            System.out.println("[INFO]: There are no Ignore zones for '" + imageName + "'.");
        }

        //---get comparison zones
        if (Files.exists(Paths.get("visualTests\\images\\" + imageName + "_Compare.png"))) {
            System.out.print("[INFO]: There are specific Areas for comparing for '" + imageName + "' - ");
            CheckScreenshot checkArea = new CheckScreenshot("visualTests\\images\\" + imageName + ".png",
                    "visualTests\\images\\" + imageName + "_Compare.png");
            checkArea.setParameters(splitX, splitY);
            checkArea.getZone(comparisonZones);
            listCompare = new ArrayList<String>(comparisonZones);
            System.out.println(listCompare.size());
        } else {
            System.out.println("[INFO]: There are no specific Areas for comparing for '" + imageName + "'.");
        }

        if (comparisonZones.size() > 0) {
            comparisonZones.removeAll(ignoreZones);
            System.out.println("[INFO]: Areas for comparing without Ignore zones " + comparisonZones.size());
        }

        //---compare images
        System.out.println("[INFO]: Check '" + imageName + "' screenshot.");
        CheckScreenshot cti2 = new CheckScreenshot("visualTests\\images\\" + imageName + ".png",
                "visualTests\\screenshots\\" + imageName + ".png");
        cti2.setParameters(splitX, splitY);
        cti2.compare3();
        if (!cti2.isIdentic()) {
            System.out.println("[INFO]: '" + imageName + "' does not match ✘");
            CheckScreenshot.saveResult(cti2.getImageResult(), imageName);
        } else {
            System.out.println("[INFO]: '" + imageName + "' match ✔");
        }
    }
}
