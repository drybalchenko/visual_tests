import Helpers.BrowsersHelper;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Test {

    static WebDriver wd = BrowsersHelper.getChromeRemoteWebDriver();

    @BeforeClass
    public static void setup() {
        wd = BrowsersHelper.getChromeRemoteWebDriver();
        wd.navigate().to("https://www.google.com");
    }

    @org.junit.Test
    public void Test_01() throws IOException {
        CheckScreenshot.checkScreenshot("Test1",  wd);
    }
}
